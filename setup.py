from setuptools import setup

setup(
    name='kbase',
    version='0.1',
    py_modules=['kbase'],
    install_requires=[
        'Click',
        'requests',
        'termcolor'
    ],
    entry_points='''
        [console_scripts]
        kbase=kbase:cli
    ''',
)