import click
import re
import json
import requests
from datetime import datetime
from termcolor import colored
from pprint import pprint


url = "https://kbase.valsdav.cloud/api/"

access_token = None
offline_token="****"


access_query = requests.post("https://auth.valsdav.cloud/auth/realms/valsdav-cloud/protocol/openid-connect/token",
                data={'client_id':'kbase','grant_type':'refresh_token', 'refresh_token':offline_token})
tokens = access_query.json()
access_token = tokens['access_token']

def parse_query(query):
    pattern_tag = r"(t:(?P<tag1>\S*)|#(?P<tag2>\S*))"
    tags = []
    matches = re.findall(pattern_tag, query)
    if matches:
        for match in matches:
            tags.append(match[1]+match[2])
            query = query.replace(match[0], '')
    text = query.strip()
    return text, tags

def do_query(text, tags, page=1):
    if text and not tags:
        query = 'items_text?aggregate={"$query_text":"' + text +'"}'
    if text and tags:
        query = 'items_text_tags?aggregate={"$query_text":"' + text +'", "$query_tags":["'+ '","'.join(tags) +'"]}'
    if not text and tags:
        query = 'items_tags?aggregate={"$query_tags":["'+ '","'.join(tags) +'"]}'
    if not text and not tags:
        query = 'items?sort=-_updated'

    #click.echo(colored(query, 'yellow', attrs=['underline']))
    r = requests.get(url + query + '&max_results=30&page={}'.format(page), headers={'Authorization':'Bearer '+ access_token})
    if r.status_code == 200:
        data = r.json()
        return data
    

def print_query_results(data):
    meta = data["_meta"]
    items = data["_items"]

    click.echo(colored(f"{meta['total']} items, page {meta['page']}/{meta['total']/30 +1:.0f}", color="yellow"))
    for ii, item in enumerate(items):
        date = datetime.strptime(item['_updated'][:-4], '%a, %d %b %Y %H:%M:%S')
        click.echo(f"{ii:>3} " + colored(f"> {item['shortid']} ","red" ) + 
                   colored(f"[{date.strftime('%d-%m-%Y %H:%M')}] ", "blue") + item['title'] +
                   colored(f"  [{','.join(sorted(item['tags']))}]", "green"))

    return meta['total']

def print_item(item):
    click.echo("-----------------------------------------------------------------------------------")
    click.echo(colored(f"> {item['title']}", "blue", attrs=["bold", "underline"]))
    click.echo(colored(f"> {','.join(item['tags'])}", "green"))
    click.echo(colored(f"> Created: {item['_created']}, Updated: {item['_updated']} \n", "cyan"))
    click.echo(item['content'] +"\n")

@click.command()
@click.argument('query', nargs=-1)
def cli(query):
    query = ' '.join(query)
    query_page = 1
    query_text, query_tags = parse_query(query)
    click.echo(colored("Query: ", 'green')+ query_text + colored("  Tags: ", "green") + ','.join(query_tags))
    
    while(True):
        data = do_query(query_text, query_tags,query_page)
        print_query_results(data)
        prompt = click.prompt(colored("#/n/q ?", "yellow"))
        
        if prompt == "q": 
            exit(0)
        elif prompt == "n":
            query_page+=1
        else:
            if " " in prompt:
                parts = prompt.split(" ")
            else:
                parts = [prompt]
            for p in parts:
                try:
                    N_item= int(p)
                    print_item(data['_items'][N_item])
                    exit(0)
                except Exception as e:
                    exit(1)
